//
//  SATScoreDecodingTests.swift
//  20190308-PareshMasani-NYCSchoolsTests
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import XCTest
@testable import _0190308_PareshMasani_NYCSchools

class SATScoreDecodingTests: XCTestCase {
    
    func testDecoding() throws {
        let url = try require(Bundle(for: SATScoreDecodingTests.self).url(forResource: "scores", withExtension: "json"))
        let data = try Data(contentsOf: url)
        let scores = try JSONDecoder().decode([SATScore].self, from: data)
        XCTAssertEqual(scores.count, 2)
        XCTAssertEqual(scores[0].dbn, "01M292")
        XCTAssertEqual(scores[0].numberOfTakers, 29)
        XCTAssertEqual(scores[0].mathAvgScore, 404)
        XCTAssertEqual(scores[0].readingAvgScore, 355)
        XCTAssertEqual(scores[0].writingAvgScore, 363)
    }
    
}
