//
//  SchoolsPresenterTests.swift
//  20190308-PareshMasani-NYCSchoolsTests
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import XCTest
import CoreLocation
import CoreData
@testable import _0190308_PareshMasani_NYCSchools

/// Covers some parts of the `SchoolsPresenter` logic.
/// Demonstrates how it can be covered with unit tests.
class SchoolsPresenterTests: XCTestCase {
    
    private var router: RouterMock!
    private var locationManager: LocationManagerMock!
    private var repository: RepositoryMock!
    private var presenter: SchoolsPresenter!
    private var view: ViewMock!
    
    override func setUp() {
        router = RouterMock()
        locationManager = LocationManagerMock()
        repository = RepositoryMock()
        view = ViewMock()
    }
    
    func testShouldStartLocationManagerOnInit() {
        presenter = SchoolsPresenter(router: router, repository: repository, locationManager: locationManager, mode: .allSchools)
        XCTAssertTrue(locationManager.isStarted)
    }
    
    func testLoadingInBookmarksMode() {
        let entity = repository.createEntity()
        entity.name = "TEST SCHOOL"
        entity.dbn = "88888"
        entity.isBookmark = true
        
        // setup presenter in bookmarks mode
        presenter = SchoolsPresenter(router: router, repository: repository, locationManager: locationManager, mode: .bookmarks)
        presenter.view = view
        
        presenter.didLoadView()
        
        // should get one school from cache
        XCTAssertEqual(presenter.numberOfSchools(), 1)
        let school = presenter.school(at: 0)
        XCTAssertEqual(school.name, "TEST SCHOOL")
        XCTAssertEqual(school.id, "88888")
        XCTAssertTrue(school.isBookmark)
        
        // should not call fetch
        XCTAssertFalse(repository.fetchCalled)
        
        // should configure the view and should not show loading indicator
        XCTAssertTrue(view.isConfiguredForBookmarks)
        XCTAssertFalse(view.didShowLoadingIndicator)
        XCTAssertFalse(view.didHideLoadingIndicator)
    }
    
    func testLoadingInAllSchoolsMode() {
        let entity = repository.createEntity()
        entity.name = "TEST SCHOOL"
        entity.dbn = "88888"
        
        // setup presenter in allSchools mode
        presenter = SchoolsPresenter(router: router, repository: repository, locationManager: locationManager, mode: .allSchools)
        presenter.view = view
        
        presenter.didLoadView()
        
        // should load one school
        XCTAssertEqual(presenter.numberOfSchools(), 1)
        let school = presenter.school(at: 0)
        XCTAssertEqual(school.name, "TEST SCHOOL")
        XCTAssertEqual(school.id, "88888")
        XCTAssertFalse(school.isBookmark)
        
        // should call fetch
        XCTAssertTrue(repository.fetchCalled)
        
        XCTAssertFalse(view.isConfiguredForBookmarks)
        
        // should show and hide loading indicator
        XCTAssertTrue(view.didShowLoadingIndicator)
        XCTAssertTrue(view.didHideLoadingIndicator)
        
        // should not show error
        XCTAssertNil(view.error)
    }
    
    func testLoadingFailure() {
        presenter = SchoolsPresenter(router: router, repository: repository, locationManager: locationManager, mode: .allSchools)
        presenter.view = view
        
        presenter.didLoadView()
        
        // should load one school
        XCTAssertEqual(presenter.numberOfSchools(), 0)
        
        // should call fetch
        XCTAssertTrue(repository.fetchCalled)
        
        // should show error (because there are no entities created)
        XCTAssertEqual(view.error, APIError.noResponse.localizedDescription)
    }
}

// -------------------------------------
// MARK: - MOCKS
// -------------------------------------

private class RouterMock: SchoolsRouterProtocol {
    func showSchoolDetails(entity: SchoolEntity, userLocation: CLLocation?) {
    }
    
    func showBookmarks() {
    }
    
    func showBoroughsFilter(selectedBoroughs: Set<Borough>?, callback: @escaping BoroughsCallback) {
    }
}

private class LocationManagerMock: LocationManagerProtocol {
    var currentLocation: CLLocation?
    var authorized: LocationAuthorizationCallback?
    var locationUpdated: LocationUpdateCallback?
    
    private(set) var isStarted = false
    
    func startUpdatingLocation() {
        isStarted = true
    }
}

private class RepositoryMock: SchoolRepositoryProtocol {
    private(set) var fetchCalled = false
    
    private(set) var entity: SchoolEntity?
    
    // I'm storing data in memory for tests
    let context = setUpInMemoryManagedObjectContext()
    
    func getCachedSchools() -> [SchoolEntity] {
        if let entity = entity {
            return [entity]
        }
        return []
    }
    
    func fetch(completion: @escaping SchoolEntityCompletion) {
        fetchCalled = true
        if let entity = entity {
            completion(.success([entity]))
        } else {
            completion(.failure(.noResponse))
        }
    }
    
    func addToBookmarks(entity: SchoolEntity) {
    }
    
    func removeFromBookmarks(entity: SchoolEntity) {
    }
    
    func createEntity() -> SchoolEntity {
        let object = NSEntityDescription.insertNewObject(forEntityName: "SchoolEntity", into: context)
        let entity = (object as? SchoolEntity).require()
        self.entity = entity
        return entity
    }
}

private class ViewMock: SchoolsViewProtocol {
    private(set) var isConfiguredForBookmarks = false
    private(set) var didShowLoadingIndicator = false
    private(set) var didHideLoadingIndicator = false
    private(set) var error: String?
    
    func configureForBookmarks() {
        isConfiguredForBookmarks = true
    }
    
    func reloadList() {
    }
    
    func setShowsBookmarksPlaceholder(_ showsPlaceholder: Bool) {
    }
    
    func showLoadingIndicator() {
        didShowLoadingIndicator = true
    }
    
    func hideLoadingIndicator() {
        didHideLoadingIndicator = true
    }
    
    func showError(_ error: String) {
        self.error = error
    }
}
