# 20190308-PareshMasani-NYCSchools

**Crucial Design Decision**

Because the APIs don't support location based sorting, I had to debat myself whether to support pagination for the list of schools or support showing nearest schools first.

While investigating; I realised that School data doesn't change often *(Last Modify was Tue, 29 May 2018 22:21:23 GMT)* and json size is 2 MB only so rather than supporting pagination, I have
decided to fetch all schools and SAT results data whenever required based on **last modified date** in response Header.

With this decision

* I was able to improve user experience. For end users, App is much quicker and slicker.
* Supported Location based sorting which is more helpful and important to the end users.
* Supported local searching of schools for each character typed which is 100x quicker than APIs calling.

Ideally, all the real life applications should have RESTful or GraphQL endpoints for sorting, filtering, location based results etc as data changes very often and data size is huge and dynamic
but NYC High Schools scenario is different. Hence, I made above decision based on what could be the best for the endusers.

**Additional Functionality Supported**
* Location based sorting
* Filter schools by Borough
* Bookmark favourite schools
* Fully Offline support
* Search by School Name or Zip
* Open Map to display direction to the school
* Attendance score is green if greater than 90% represents solid school standard, between 80-90% yellow color, and below 80% red color displayed
* Contact school by phone, email, or their website

**Important Points**

* App architecture is MVP, and doesn't use any third-party libraries.
* I have written only few unit tests to demonstrate my capability of writing unit tests.
* Implemented LoadingViewController in Objective-C to demonstrate Obj-C and Swift interoperability.
* Schools sorted Aphabatically if Location Permission is not given
* Cell height is dynamic based on School Name length.
* Minimum suported iOS version is iOS 11.0

**What could be improved in the App?**

* Get Unit tests coverage at least 80-90%. 
* UI tests
* More granular filter options could be supported like filter by attendance rate or any other important fields
* Map View and List View toggle support. This app only supports List View to display schools but Map View can be supported which will look much slicker to user in terms of nearest schools on the Map. 

