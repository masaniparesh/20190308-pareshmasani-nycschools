//
//  AppDelegate.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    private let coreDataManager = CoreDataManager.default
    
    private var router: Router!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupAppearance()
        let navigationController = (window?.rootViewController as? UINavigationController).require()
        router = Router(navigationController: navigationController)
        router.showSchools()
        return true
    }
    
    private func setupAppearance() {
        UINavigationBar.appearance().barTintColor = .themeColor
        UINavigationBar.appearance().tintColor = .navigationBarTintColor
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor : UIColor.navigationBarTintColor]
        UITableViewCell.appearance().tintColor = .themeColor
        UIButton.appearance().tintColor = .themeColor
        UIButton.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = .navigationBarTintColor
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        coreDataManager.saveContext()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        coreDataManager.saveContext()
    }
    
}
