//
//  APIResponse.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

struct APIResponse<Value> {
    /// The result of response serialization.
    let result: APIResult<Value>
    
    /// The server's response to the URL request.
    let httpResponse: HTTPURLResponse?
    
    init(result: APIResult<Value>, httpResponse: HTTPURLResponse? = nil) {
        self.result = result
        self.httpResponse = httpResponse
    }
}

extension APIResponse {
    func map<T>(_ transform: (Value) throws -> T) -> APIResponse<T> {
        return APIResponse<T>(result: result.map(transform), httpResponse: httpResponse)
    }
    
    var headers: [String: String]? {
        return httpResponse?.allHeaderFields as? [String: String]
    }
    
    var lastModifiedDate: Date? {
        return headers?["Last-Modified"].flatMap(DateFormatter.rfc2822.date)
    }
}
