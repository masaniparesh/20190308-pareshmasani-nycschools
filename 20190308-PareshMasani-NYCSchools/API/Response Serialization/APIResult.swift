//
//  APIResult.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

/// Used to represent whether a request was successful or encountered an error.
enum APIResult<Value> {
    case success(Value)
    case failure(APIError)
}

extension APIResult {
    var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    var error: APIError? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
    
    /// Evaluates the specified closure when the `APIResult` is a success, passing the unwrapped value as a parameter.
    func map<T>(_ transform: (Value) throws -> T) -> APIResult<T> {
        switch self {
        case .success(let value):
            do {
                return try .success(transform(value))
            } catch let error as APIError {
                return .failure(error)
            } catch let error {
                return .failure(.serializationFailed(error))
            }
        case .failure(let error):
            return .failure(error)
        }
    }
}
