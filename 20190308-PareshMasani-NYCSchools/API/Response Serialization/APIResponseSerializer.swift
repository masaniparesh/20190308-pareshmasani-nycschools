//
//  APIResponseSerializer.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

/// Reponsible for serializing response from server.
class APIResponseSerializer {
    
    /// Status code is required to be in the range of `validStatusCodes`.
    let validStatusCodes = 200...299
    
    func serializeResponse<Value>(data: Data?,
                                  response: URLResponse?,
                                  error: Error?,
                                  transform: (Data) throws -> Value) -> APIResponse<Value> {
        return serializeDataResponse(data: data, response: response, error: error).map(transform)
    }
    
    private func serializeDataResponse(data: Data?,
                                       response: URLResponse?,
                                       error: Error?) -> APIResponse<Data> {
        guard let httpResponse = response as? HTTPURLResponse else {
            let apiError: APIError
            if let urlError = error as? URLError {
                apiError = APIError(urlError: urlError)
            } else {
                apiError = .noResponse
            }
            return APIResponse(result: .failure(apiError))
        }
        guard validStatusCodes ~= httpResponse.statusCode else {
            let apiError = APIError(statusCode: httpResponse.statusCode)
            return APIResponse(result: .failure(apiError))
        }
        guard let data = data else {
            return APIResponse(result: .failure(.noResponseData))
        }
        return APIResponse(result: .success(data), httpResponse: httpResponse)
    }
}
