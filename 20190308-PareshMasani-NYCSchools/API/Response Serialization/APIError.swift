//
//  APIError.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

enum APIError: Error {
    case noURL
    case lostConnection
    case notConnected
    case timedOut
    /// No response was received from server, but we don't have any error details.
    case noResponse
    case noResponseData
    case badRequest // 400
    case unauthorized // 401
    case forbidden // 403
    case notFound // 404
    case serverError // 500
    case serializationFailed(Error)
    case other
}

extension APIError {
    var localizedDescription: String {
        switch self {
        case .noURL:
            return "Couldn't construct URL"
        case .lostConnection:
            return "Lost connection"
        case .notConnected:
            return "Not connected to Internet"
        case .timedOut:
            return "A request timed out."
        case .noResponse:
            return "No response was received from server"
        case .noResponseData:
            return "No data was received from server."
        case .badRequest:
            return "Probably your request was malformed. See the error message in the body for details"
        case .unauthorized:
            return "You attempted to authenticate but something went wrong."
        case .forbidden:
            return "You’re not authorized to access this resource"
        case .notFound:
            return "The resource requested doesn’t exist"
        case .serverError:
            return "Something has gone wrong with Socrta’s platform."
        case .serializationFailed(let error):
            return error.localizedDescription
        case .other:
            return "Uknown error happened"
        }
    }
}

extension APIError {
    init(urlError: URLError) {
        switch urlError.code {
        case .timedOut:
            self = .timedOut
        case .notConnectedToInternet:
            self = .notConnected
        case .networkConnectionLost:
            self = .lostConnection
        default:
            self = .other
        }
    }
    
    init(statusCode: Int) {
        switch statusCode {
        case 400:
            self = .badRequest
        case 401:
            self = .unauthorized
        case 403:
            self = .forbidden
        case 404:
            self = .notFound
        case 500:
            self = .serverError
        default:
            self = .other
        }
    }
}
