//
//  NetworkingManager.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

typealias APICompletion<T> = (APIResponse<T>) -> Void
typealias DateCompletion = (Date?) -> Void

protocol Networking {
    func getSchools(completion: @escaping APICompletion<[School]>)
    func getSATScores(completion: @escaping APICompletion<[SATScore]>)
    func getSchoolsLastModifiedDate(completion: @escaping DateCompletion)
    func getSATScoresLastModifiedDate(completion: @escaping DateCompletion)
}

final class NetworkingManager: Networking {
    
    private enum Const {
        static let baseURL = "https://data.cityofnewyork.us/resource/"
        static let schoolsResource = "s3k6-pzi2"
        static let scoresResource = "f9bf-2cp4"
        static let limit = "$limit"
    }
    
    private let session: URLSession
    private let decoder: JSONDecoder
    private let serializer = APIResponseSerializer()
    
    init(session: URLSession = .shared, decoder: JSONDecoder = .init()) {
        self.session = session
        self.decoder = decoder
    }
    
    func getSchools(completion: @escaping APICompletion<[School]>) {
        getResource(Const.schoolsResource, completion: completion)
    }
    
    func getSATScores(completion: @escaping APICompletion<[SATScore]>) {
        getResource(Const.scoresResource, completion: completion)
    }
    
    func getSchoolsLastModifiedDate(completion: @escaping DateCompletion) {
        getResource(Const.schoolsResource, limit: 1) { (response: APIResponse<[School]>) in
            completion(response.lastModifiedDate)
        }
    }
    
    func getSATScoresLastModifiedDate(completion: @escaping DateCompletion) {
        getResource(Const.scoresResource, limit: 1) { (response: APIResponse<[SATScore]>) in
            completion(response.lastModifiedDate)
        }
    }
}

private extension NetworkingManager {
    func getResource<T: Decodable>(_ resource: String, limit: Int = 0, completion: @escaping APICompletion<T>) {
        guard let url = constructURL(resource: resource, limit: limit) else {
            return completion(APIResponse(result: .failure(.noURL)))
        }
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            let serializedResponse = self.serializer.serializeResponse(
                data: data,
                response: response,
                error: error,
                transform: { data -> T in
                    return try self.decoder.decode(T.self, from: data)
            })
            executeOnMainThread {
                completion(serializedResponse)
            }
        }
        dataTask.resume()
    }
    
    func constructURL(resource: String, limit: Int) -> URL? {
        var components = URLComponents(string: "\(Const.baseURL + resource).json")
        if limit > 0 {
            components?.queryItems = [URLQueryItem(name: Const.limit, value: "\(limit)")]
        }
        return components?.url
    }
}
