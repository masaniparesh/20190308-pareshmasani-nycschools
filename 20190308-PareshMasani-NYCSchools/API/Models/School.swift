//
//  School.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

// https://data.cityofnewyork.us/resource/s3k6-pzi2.json
struct School: Decodable {
    enum DecodeError: Error {
        case incorrectType
    }
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case email = "school_email"
        case phone = "phone_number"
        case zip
        case attendanceRate = "attendance_rate"
        case website
        case borough
        case latitude
        case longitude
        case location
        case totalStudens = "total_students"
        case finalGrades = "finalgrades"
        case overview = "overview_paragraph"
    }
    
    let dbn: String
    let name: String
    let email: String?
    let phone: String?
    let zip: Int
    let attendanceRate: Double?
    let website: String?
    let borough: String?
    let latitude: Double?
    let longitude: Double?
    let location: String?
    let totalStudens: Int
    let finalGrades: String?
    let overview: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try container.decode(String.self, forKey: .dbn)
        name = try container.decode(String.self, forKey: .name)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        phone = try container.decodeIfPresent(String.self, forKey: .phone)
        // If Zip is not available then decoding will fail
        zip = try Int(container.decode(String.self, forKey: .zip)).orThrow(DecodeError.incorrectType)
        // Mapping optional String value to Double
        attendanceRate = try container.decodeIfPresent(String.self, forKey: .attendanceRate).flatMap(Double.init)
        website = try container.decodeIfPresent(String.self, forKey: .website)
        // sometimes there is space after borough name in the response
        borough = try container.decodeIfPresent(String.self, forKey: .borough)?.trimmingCharacters(in: .whitespaces)
        latitude = try container.decodeIfPresent(String.self, forKey: .latitude).flatMap(Double.init)
        longitude = try container.decodeIfPresent(String.self, forKey: .longitude).flatMap(Double.init)
        location = try container.decodeIfPresent(String.self, forKey: .location)
        totalStudens = try Int(container.decode(String.self, forKey: .totalStudens)).orThrow(DecodeError.incorrectType)
        finalGrades = try container.decodeIfPresent(String.self, forKey: .finalGrades)
        overview = try container.decode(String.self, forKey: .overview)
    }
}
