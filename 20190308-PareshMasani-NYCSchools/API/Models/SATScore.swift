//
//  SATScore.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 08/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

// https://data.cityofnewyork.us/resource/f9bf-2cp4.json
struct SATScore: Decodable {
    private enum CodingKeys: String, CodingKey {
        case dbn
        case numberOfTakers = "num_of_sat_test_takers"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
    
    let dbn: String
    let numberOfTakers: Int?
    let readingAvgScore: Int?
    let mathAvgScore: Int?
    let writingAvgScore: Int?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try container.decode(String.self, forKey: .dbn)
        // response contains strings even for number values
        // Mapping optional String value to Int
        numberOfTakers = try container.decodeIfPresent(String.self, forKey: .numberOfTakers).flatMap(Int.init)
        readingAvgScore = try container.decodeIfPresent(String.self, forKey: .readingAvgScore).flatMap(Int.init)
        mathAvgScore = try container.decodeIfPresent(String.self, forKey: .mathAvgScore).flatMap(Int.init)
        writingAvgScore = try container.decodeIfPresent(String.self, forKey: .writingAvgScore).flatMap(Int.init)
    }
}
