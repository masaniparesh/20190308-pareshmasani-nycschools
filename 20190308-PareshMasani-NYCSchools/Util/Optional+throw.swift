//
//  Optional+throw.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

extension Optional {
    func orThrow(
        _ errorExpression: @autoclosure () -> Error
        ) throws -> Wrapped {
        guard let value = self else {
            throw errorExpression()
        }
        
        return value
    }
}
