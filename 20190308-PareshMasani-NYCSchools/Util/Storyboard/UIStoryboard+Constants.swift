//
//  UIStoryboard+Constants.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    enum Name: String {
        case schools = "Schools"
        case boroughs = "Boroughs"
        case details = "SchoolDetails"
    }
    
    convenience init(storyboard: Name, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    class func storyboard(_ storyboard: Name, bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: bundle)
    }
    
    func instantiateViewController<T: UIViewController>() -> T where T: StoryboardIdentifiable {
        guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }
        return viewController
    }
}
