//
//  CLLocation+Helpers.swift
//  DiningClubGroup
//
//  Created by Paresh Masani on 9/2/18.
//  Copyright © 2018 Hi Mum Said Dad. All rights reserved.
//

import CoreLocation

extension CLLocationDistance {
    var miles: Double {
        return self / 1609.344
    }
}

