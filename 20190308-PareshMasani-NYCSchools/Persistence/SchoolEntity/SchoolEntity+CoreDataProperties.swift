//
//  SchoolEntity+CoreDataProperties.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//
//

import Foundation
import CoreData


extension SchoolEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SchoolEntity> {
        return NSFetchRequest<SchoolEntity>(entityName: "SchoolEntity")
    }

    @NSManaged public var dbn: String
    @NSManaged public var name: String
    @NSManaged public var email: String?
    @NSManaged public var phone: String?
    @NSManaged public var zip: Int32
    @NSManaged public var attendanceRate: Double
    @NSManaged public var website: String?
    @NSManaged public var borough: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var location: String?
    @NSManaged public var totalStudens: Int32
    @NSManaged public var finalGrades: String?
    @NSManaged public var overview: String?
    @NSManaged public var numberOfSATTakers: Int32
    @NSManaged public var readingAvgScore: Int16
    @NSManaged public var mathAvgScore: Int16
    @NSManaged public var writingAvgScore: Int16
    @NSManaged public var isBookmark: Bool

}
