//
//  SchoolEntity+CoreDataClass.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//
//

import Foundation
import CoreData
import CoreLocation

@objc(SchoolEntity)
public class SchoolEntity: NSManagedObject {
    
    func populate(withSchool school: School) {
        dbn = school.dbn
        name = school.name
        email = school.email
        phone = school.phone
        zip = Int32(school.zip)
        attendanceRate = school.attendanceRate ?? 0
        website = school.website
        borough = school.borough
        latitude = school.latitude ?? 0
        longitude = school.longitude ?? 0
        location = school.location
        totalStudens = Int32(school.totalStudens)
        finalGrades = school.finalGrades
        overview = school.overview
    }
    
    func populateScore(_ score: SATScore) {
        numberOfSATTakers = Int32(score.numberOfTakers ?? 0)
        mathAvgScore = Int16(score.mathAvgScore ?? 0)
        readingAvgScore = Int16(score.readingAvgScore ?? 0)
        writingAvgScore = Int16(score.writingAvgScore ?? 0)
    }
    
    var coordinate: CLLocationCoordinate2D? {
        guard latitude != 0, longitude != 0 else { return nil }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

}
