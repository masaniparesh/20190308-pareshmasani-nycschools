//
//  SchoolSortDescriptor.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import CoreLocation

enum SchoolSortDescriptor {
    case name
    case distance(to: CLLocationCoordinate2D)
}

extension SchoolSortDescriptor {
    func predicate(_ lhs: SchoolEntity, _ rhs: SchoolEntity) -> Bool {
        switch self {
        case .name:
            return lhs.name < rhs.name
        case .distance(let coordinate):
            let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            return lhs.distance(to: location) < rhs.distance(to: location)
        }
    }
}

extension SchoolEntity {
    func distance(to location: CLLocation) -> CLLocationDistance {
        guard let coordinate = coordinate else { return .greatestFiniteMagnitude }
        return location.distance(from: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
}
