//
//  SchoolFilterDescriptor.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

enum SchoolFilterDescriptor {
    case nameContains(String)
    case zipHasPrefix(Int)
    case inBorougs(Set<Borough>)
    case isBookmark
}

extension SchoolFilterDescriptor {
    func predicate(_ school: SchoolEntity) -> Bool {
        switch self {
        case .nameContains(let string):
            return school.name.range(of: string, options: .caseInsensitive) != nil
        case .zipHasPrefix(let prefix):
            return String(school.zip).hasPrefix(String(prefix))
        case .inBorougs(let boroughs):
            guard let borough = school.borough else { return false }
            return boroughs.contains { $0.rawValue == borough }
        case .isBookmark:
            return school.isBookmark
        }
    }
}
