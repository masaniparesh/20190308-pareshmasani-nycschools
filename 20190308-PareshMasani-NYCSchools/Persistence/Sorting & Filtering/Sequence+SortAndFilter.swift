//
//  Sequence+SortAndFilter.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

extension Sequence where Element == SchoolEntity {
    
    func filter(using filtersDescriptors: [SchoolFilterDescriptor]) -> [Element] {
        if filtersDescriptors.isEmpty {
            return self.map { $0 }
        }
        return filter { school in
            let isNotIncluded = filtersDescriptors.contains { !$0.predicate(school) }
            return !isNotIncluded
        }
    }
    
    func sorted(using descriptor: SchoolSortDescriptor) -> [Element] {
        return sorted(by: descriptor.predicate)
    }
    
    func apply(filters: [SchoolFilterDescriptor], sortDescriptor: SchoolSortDescriptor) -> [Element] {
        return filter(using: filters).sorted(using: sortDescriptor)
    }
    
}
