//
//  SchoolRepository.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import CoreData

typealias SchoolEntityCompletion = (APIResult<[SchoolEntity]>) -> Void

protocol SchoolRepositoryProtocol {
    func getCachedSchools() -> [SchoolEntity]
    func fetch(completion: @escaping SchoolEntityCompletion)
    func addToBookmarks(entity: SchoolEntity)
    func removeFromBookmarks(entity: SchoolEntity)
}

class SchoolRepository: SchoolRepositoryProtocol {
    private let networking: Networking
    private let dataContext: NSManagedObjectContext
    private let defaults: UserDefaults
    
    // Dependency injection makes it easy to mock objects
    init(networking: Networking = NetworkingManager(),
         dataContext: NSManagedObjectContext = CoreDataManager.default.persistentContainer.viewContext,
         defaults: UserDefaults = .standard) {
        self.networking = networking
        self.dataContext = dataContext
        self.defaults = defaults
    }
    
    func getCachedSchools() -> [SchoolEntity] {
        let request: NSFetchRequest<SchoolEntity> = SchoolEntity.fetchRequest()
        do {
            return try dataContext.fetch(request)
        } catch {
            return []
        }
    }
    
    func fetch(completion: @escaping SchoolEntityCompletion) {
        // I'm using this queue to assign error synchronously
        let errorQueue = DispatchQueue(label: "SchoolRepository.errorQueue")

        let group = DispatchGroup()
        var entities: [SchoolEntity] = []
        var scores: [SATScore] = []
        var apiError: APIError?
        
        group.enter()
        loadSchools { result in
            entities = result.value ?? []
            errorQueue.sync {
                apiError = apiError ?? result.error
            }
            group.leave()
        }
        
        group.enter()
        loadScores { result in
            scores = result.value ?? []
            errorQueue.sync {
                apiError = apiError ?? result.error
            }
            group.leave()
        }
        
        group.notify(queue: .main) {
            // schools and scores requests are completed at this point
            if let error = apiError {
                return completion(.failure(error))
            }
            self.udpateScores(scores, forEntities: entities)
            self.saveContextIfNeeded()
            completion(.success(entities))
        }
    }
    
    func addToBookmarks(entity: SchoolEntity) {
        entity.isBookmark = true
        saveContextIfNeeded()
    }
    
    func removeFromBookmarks(entity: SchoolEntity) {
        entity.isBookmark = false
        saveContextIfNeeded()
    }
}

private extension SchoolRepository {
    func saveContextIfNeeded() {
        if dataContext.hasChanges {
            try? dataContext.save()
        }
    }
    
    func loadSchools(completion: @escaping SchoolEntityCompletion) {
        validateSchoolsLastModifiedDate { validated in
            guard !validated else {
                let entities = self.getCachedSchools()
                return completion(.success(entities))
            }
            self.networking.getSchools { response in
                switch response.result {
                case .success(let schools):
                    let entities = self.cacheSchools(schools)
                    completion(.success(entities))
                case .failure(let error):
                    completion(.failure(error))
                }
                if let date = response.lastModifiedDate {
                    self.defaults.schoolsLastModifiedDate = date
                }
            }
        }
    }
    
    func loadScores(completion: @escaping (APIResult<[SATScore]>) -> Void) {
        validateScoresLastModifiedDate { validated in
            guard !validated else {
                return completion(.success([]))
            }
            self.networking.getSATScores { response in
                switch response.result {
                case .success(let scores):
                    completion(.success(scores))
                case .failure(let error):
                    completion(.failure(error))
                }
                if let date = response.lastModifiedDate {
                    self.defaults.scoresLastModifiedDate = date
                }
            }
        }
    }

    func validateSchoolsLastModifiedDate(completion: @escaping (Bool) -> Void) {
        guard let lastModified = defaults.schoolsLastModifiedDate else {
            return completion(false)
        }
        networking.getSchoolsLastModifiedDate { date in
            guard let date = date else {
                return completion(false)
            }
            self.defaults.schoolsLastModifiedDate = date
            completion(date == lastModified)
        }
    }
    
    func validateScoresLastModifiedDate(completion: @escaping (Bool) -> Void) {
        guard let lastModified = defaults.scoresLastModifiedDate else {
            return completion(false)
        }
        networking.getSATScoresLastModifiedDate { date in
            guard let date = date else {
                return completion(false)
            }
            self.defaults.scoresLastModifiedDate = date
            completion(date == lastModified)
        }
    }
    
    func getFavoriteSchools() -> [SchoolEntity] {
        let request: NSFetchRequest<SchoolEntity> = SchoolEntity.fetchRequest()
        request.predicate = NSPredicate(format: "isBookmark == YES")
        do {
            return try dataContext.fetch(request)
        } catch {
            return []
        }
    }
    
    func cacheSchools(_ schools: [School]) -> [SchoolEntity] {
        let bookmarkIdentifiers = Set(getFavoriteSchools().map { $0.dbn })
        clearCache()
        return schools.map { school in
            let entity = NSEntityDescription.insertNewObject(forEntityName: "\(SchoolEntity.self)", into: dataContext) as! SchoolEntity
            entity.populate(withSchool: school)
            entity.isBookmark = bookmarkIdentifiers.contains(school.dbn)
            return entity
        }
    }
    
    func clearCache() {
        getCachedSchools().forEach { entity in
            dataContext.delete(entity)
        }
    }
    
    func udpateScores(_ scores: [SATScore], forEntities entities: [SchoolEntity]) {
        let scoresMap = Dictionary(grouping: scores) { $0.dbn }
        entities.forEach { entity in
            if let score = scoresMap[entity.dbn]?.first {
                entity.populateScore(score)
            }
        }
    }
}

// -------------------------------------
// MARK: - UserDefaults
// -------------------------------------
private extension UserDefaults {
    private enum Const {
        static let schoolsLastModifiedDate = "Last-Modified-Schools"
        static let scoresLastModifiedDate = "Last-Modified-Scores"
    }
    
    var schoolsLastModifiedDate: Date? {
        get {
            return getDate(forKey: Const.schoolsLastModifiedDate)
        }
        set {
            set(date: newValue, forKey: Const.schoolsLastModifiedDate)
            synchronize()
        }
    }
    
    var scoresLastModifiedDate: Date? {
        get {
            return getDate(forKey: Const.scoresLastModifiedDate)
        }
        set {
            set(date: newValue, forKey: Const.scoresLastModifiedDate)
            synchronize()
        }
    }
    
    private func getDate(forKey key: String) -> Date? {
        if let interval = value(forKey: key) as? TimeInterval {
            return Date(timeIntervalSince1970: interval)
        }
        return nil
    }
    
    private func set(date: Date?, forKey key: String) {
        set(date?.timeIntervalSince1970, forKey: key)
    }
}
