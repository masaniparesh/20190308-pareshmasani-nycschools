//
//  UIColor+Const.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

@objc extension UIColor {
    static let themeColor = #colorLiteral(red: 0.3743038774, green: 0.3974404037, blue: 0.4601732492, alpha: 1)
    static let navigationBarTintColor = UIColor.white
    static let normalAttendanceRate = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
    static let badAttendanceRate = UIColor.red
    static let goodAttedanceRate = #colorLiteral(red: 0.4338786602, green: 0.8531938791, blue: 0.2693812847, alpha: 1)
}
