//
//  LocationManager.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import CoreLocation

typealias LocationAuthorizationCallback = () -> Void
typealias LocationUpdateCallback = (CLLocation?) -> Void

protocol LocationManagerProtocol: class {
    var currentLocation: CLLocation? { get }
    var authorized: LocationAuthorizationCallback? { get set }
    var locationUpdated: LocationUpdateCallback? { get set }
    func startUpdatingLocation()
}

/// Wrapper around CLLocationManager
class LocationManager: NSObject, LocationManagerProtocol {
    
    private let mananager = CLLocationManager()
    
    var currentLocation: CLLocation? {
        return mananager.location
    }
    
    var authorized: LocationAuthorizationCallback?
    var locationUpdated: LocationUpdateCallback?
    
    override init() {
        super.init()
        mananager.delegate = self
    }
    
    func startUpdatingLocation() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            mananager.requestWhenInUseAuthorization()
        } else {
            mananager.startUpdatingLocation()
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            mananager.startUpdatingLocation()
            authorized?()
        case .denied, .notDetermined, .restricted:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationUpdated?(currentLocation)
    }
}
