//
//  Router.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import SafariServices

protocol SchoolsRouterProtocol {
    func showSchoolDetails(entity: SchoolEntity, userLocation: CLLocation?)
    func showBookmarks()
    func showBoroughsFilter(selectedBoroughs: Set<Borough>?, callback: @escaping BoroughsCallback)
}

protocol SchoolDetailRouterProtocol {
    func openSafari(_ url: URL)
}

/// Contains navigation logic
class Router: SchoolsRouterProtocol, SchoolDetailRouterProtocol {
    
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func showSchools() {
        let viewController = SceneAssembly.assemblyScene(.shools(router: self))
        navigationController.pushViewController(viewController, animated: false)
    }
    
    func showSchoolDetails(entity: SchoolEntity, userLocation: CLLocation?) {
        let viewController = SceneAssembly.assemblyScene(.details(entity: entity, userLocation: userLocation, router: self))
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func showBookmarks() {
        let navVC = UINavigationController()
        let router = Router(navigationController: navVC)
        let viewController = SceneAssembly.assemblyScene(.bookmarks(router: router))
        navVC.pushViewController(viewController, animated: false)
        navigationController.present(navVC, animated: true, completion: nil)
    }
    
    func showBoroughsFilter(selectedBoroughs: Set<Borough>?, callback: @escaping BoroughsCallback) {
        let viewController = SceneAssembly.assemblyScene(.boroughs(selectedBoroughs: selectedBoroughs, callback: callback))
        let navVC = UINavigationController(rootViewController: viewController)
        navigationController.present(navVC, animated: true, completion: nil)
    }
    
    func openSafari(_ url: URL) {
        let viewController = SFSafariViewController(url: url)
        navigationController.present(viewController, animated: true, completion: nil)
    }
}
