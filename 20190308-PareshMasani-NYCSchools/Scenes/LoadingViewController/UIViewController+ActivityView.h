//
//  UIViewController+ActivityView.h
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (ActivityView)

- (void)showActivityView;
- (void)hideActivityView;

@end

NS_ASSUME_NONNULL_END
