//
//  LoadingViewController.m
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

#import "LoadingViewController.h"
#import "_0190308_PareshMasani_NYCSchools-Swift.h"
@import UIKit;

@interface LoadingViewController ()
@property (nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.userInteractionEnabled = NO;
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.color = UIColor.themeColor;
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.activityIndicator];
    [self.activityIndicator.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.activityIndicator.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak LoadingViewController *weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf.activityIndicator startAnimating];
    });
}

@end
