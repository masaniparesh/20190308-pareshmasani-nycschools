//
//  UIViewController+ActivityView.m
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

#import "UIViewController+ActivityView.h"
#import "LoadingViewController.h"
#import <objc/runtime.h>

static void * const associatedKey = (void*)&associatedKey;

@interface UIViewController ()
@property (nonatomic) LoadingViewController *activityViewController;
@end

@implementation UIViewController (ActivityView)

- (LoadingViewController *)activityViewController {
    return objc_getAssociatedObject(self, associatedKey);
}

- (void)setActivityViewController:(LoadingViewController *)activityViewController {
    objc_setAssociatedObject(self, associatedKey, activityViewController, OBJC_ASSOCIATION_RETAIN);
}

- (void)showActivityView {
    if (self.activityViewController == nil) {
        self.activityViewController = [LoadingViewController new];
    }
    [self addChildViewController:self.activityViewController];
    [self.view addSubview:self.activityViewController.view];
    self.activityViewController.view.frame = self.view.bounds;
    [self.activityViewController didMoveToParentViewController:self];
}

- (void)hideActivityView {
    [self.activityViewController willMoveToParentViewController:nil];
    [self.activityViewController removeFromParentViewController];
    [self.activityViewController.view removeFromSuperview];
}

@end
