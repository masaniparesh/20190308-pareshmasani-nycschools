//
//  SchoolListViewModel.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

/// View model for thelist/map view
struct SchoolViewModel {
    let id: String
    let name: String
    let borough: String
    let isBookmark: Bool
    let distanceToUser: String?
}

/// View model for the detail view
struct SchoolDetailViewModel {
    let name: String
    let address: String
    let distanceToUser: String?
    let mathScore: String
    let readingScore: String
    let writingScore: String
    let testTakers: String
    let totalStudents: String
    let attendancePercentage: String
    let attendancePercentageColor: UIColor
    let grades: String
    let overview: String
    let isBookmark: Bool
}
