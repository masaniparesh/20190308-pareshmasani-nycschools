//
//  BoroughsPresenter.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

protocol BoroughsPresenterProtocol {
    func numberOfItems() -> Int
    func title(at index: Int) -> String
    func isSelected(at index: Int) -> Bool
    func didSelectItem(at index: Int)
    func didTapDone()
}

typealias BoroughsCallback = (Set<Borough>) -> Void

final class BoroughsPresenter: BoroughsPresenterProtocol {
    
    weak var view: BoroughsViewProtocol?
    
    private var selectedBorougs: Set<Borough>
    private let callback: BoroughsCallback
    
    init(selectedBorougs: Set<Borough>?, callback: @escaping BoroughsCallback) {
        self.selectedBorougs = selectedBorougs ?? []
        self.callback = callback
    }
    
    func numberOfItems() -> Int {
        return Borough.allCases.count + 1
    }
    
    func title(at index: Int) -> String {
        if index == 0 {
            return "All"
        }
        return Borough.allCases[index - 1].displayValue
    }
    
    func isSelected(at index: Int) -> Bool {
        let allBoroughsSelected = selectedBorougs.isEmpty || selectedBorougs.count == Borough.allCases.count
        if index == 0 {
            return allBoroughsSelected
        }
        if allBoroughsSelected {
            return false
        } else {
            return selectedBorougs.contains(Borough.allCases[index - 1])
        }
    }
    
    func didSelectItem(at index: Int) {
        if index == 0 {
            // user selected "All"
            selectedBorougs.removeAll()
        } else {
            // toggle selection
            let borough = Borough.allCases[index - 1]
            if selectedBorougs.contains(borough) {
                selectedBorougs.remove(borough)
            } else {
                selectedBorougs.insert(borough)
            }
        }
        view?.reload()
    }
    
    func didTapDone() {
        callback(selectedBorougs)
    }
    
}
