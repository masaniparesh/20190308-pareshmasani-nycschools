//
//  Borough.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation

// Row values correspond to what we receive in response
enum Borough: String, CaseIterable {
    case manhattan = "MANHATTAN"
    case brooklyn = "BROOKLYN"
    case queens = "QUEENS"
    case bronx = "BRONX"
    case statenIsland = "STATEN IS"
}

extension Borough {
    var displayValue: String {
        switch self {
        case .manhattan:
            return "Manhattan"
        case .brooklyn:
            return "Brooklyn"
        case .queens:
            return "Queens"
        case .bronx:
            return "Bronx"
        case .statenIsland:
            return "Staten Island"
        }
    }
}
