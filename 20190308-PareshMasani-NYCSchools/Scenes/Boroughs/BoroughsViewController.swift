//
//  BoroughsViewController.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

protocol BoroughsViewProtocol: class {
    func reload()
}

final class BoroughsViewController: UITableViewController, BoroughsViewProtocol {
    
    var presenter: BoroughsPresenterProtocol!
    
    private let cellReuseIdentifier = "Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // If had more time, would use Localizable.strings with swiftgen
        title = "Select Boroughs"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
    }
    
    func reload() {
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) ?? UITableViewCell(style: .default, reuseIdentifier: cellReuseIdentifier)
        cell.textLabel?.text = presenter.title(at: indexPath.row)
        cell.accessoryType = presenter.isSelected(at: indexPath.row) ? .checkmark : .none
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItems()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectItem(at: indexPath.row)
    }
    
    @objc private func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func doneAction() {
        presenter.didTapDone()
        dismiss(animated: true, completion: nil)
    }
}
