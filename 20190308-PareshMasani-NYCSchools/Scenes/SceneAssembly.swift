//
//  SceneAssembly.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct SceneAssembly {
    
    enum Scene {
        case shools(router: SchoolsRouterProtocol)
        case bookmarks(router: SchoolsRouterProtocol)
        case boroughs(selectedBoroughs: Set<Borough>?, callback: BoroughsCallback)
        case details(entity: SchoolEntity, userLocation: CLLocation?, router: SchoolDetailRouterProtocol)
    }
    
    static func assemblyScene(_ scene: Scene) -> UIViewController {
        switch scene {
        case .shools(let router):
            let viewController: SchoolsViewController = UIStoryboard(storyboard: .schools).instantiateViewController()
            let presenter = SchoolsPresenter(router: router)
            viewController.presenter = presenter
            presenter.view = viewController
            return viewController
        case .bookmarks(let router):
            let viewController: SchoolsViewController = UIStoryboard(storyboard: .schools).instantiateViewController()
            let presenter = SchoolsPresenter(router: router, mode: .bookmarks)
            viewController.presenter = presenter
            presenter.view = viewController
            return viewController
        case .boroughs(let selectedBoroughs, let callback):
            let viewController = BoroughsViewController()
            let presenter = BoroughsPresenter(selectedBorougs: selectedBoroughs, callback: callback)
            viewController.presenter = presenter
            presenter.view = viewController
            return viewController
        case .details(let entity, let location, let router):
            let viewController: SchoolsDetailsViewController = UIStoryboard(storyboard: .details).instantiateViewController()
            let presenter = SchoolDetailsPresenter(entity: entity, router: router, userLocation: location)
            viewController.presenter = presenter
            presenter.view = viewController
            return viewController
        }
    }
    
}
