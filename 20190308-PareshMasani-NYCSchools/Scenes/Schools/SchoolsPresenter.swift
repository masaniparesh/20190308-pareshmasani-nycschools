//
//  SchoolsPresenter.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import CoreLocation

protocol SchoolsPresenterProtocol: class {
    func didLoadView()
    func numberOfSchools() -> Int
    func school(at index: Int) -> SchoolViewModel
    func didSelectSchool(at index: Int)
    func didBookmarkSchool(at index: Int)
    func didUnbookmarkSchool(at index: Int)
    func didChangeSearchString(_ searchString: String?)
    func didTapFilterButton()
    func didTapBookmarksButton()
    func didRefresh()
}

final class SchoolsPresenter: SchoolsPresenterProtocol {
    
    weak var view: SchoolsViewProtocol?
    
    let router: SchoolsRouterProtocol
    let repository: SchoolRepositoryProtocol
    let locationManager: LocationManagerProtocol
    
    enum Mode {
        case allSchools
        case bookmarks
    }
    
    let mode: Mode
    
    private var schoolEntities: [SchoolEntity] = [] {
        didSet {
            updateViewModels()
        }
    }
    
    private var searchFilter: SchoolFilterDescriptor? {
        didSet {
            updateViewModels()
        }
    }
    
    private var selectedBoroughs: Set<Borough>? {
        didSet {
            updateViewModels()
        }
    }
    
    private var userLocation: CLLocation?
    
    private var viewModels: [SchoolViewModel] = [] {
        didSet {
            if mode == .bookmarks {
                view?.setShowsBookmarksPlaceholder(viewModels.isEmpty)
            }
            view?.reloadList()
        }
    }
    
    init(router: SchoolsRouterProtocol,
         repository: SchoolRepositoryProtocol = SchoolRepository(),
         locationManager: LocationManagerProtocol = LocationManager(),
         mode: Mode = .allSchools) {
        self.router = router
        self.repository = repository
        self.locationManager = locationManager
        self.mode = mode
        userLocation = locationManager.currentLocation
        startLocationManager()
    }
    
    func didLoadView() {
        schoolEntities = repository.getCachedSchools()
        
        // don't fetch data if we're in "bookmarks" mode
        guard mode == .allSchools else {
            view?.configureForBookmarks()
            return
        }
        
        view?.showLoadingIndicator()
        repository.fetch { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let entities):
                self.schoolEntities = entities
            case .failure(let error):
                self.view?.showError(error.localizedDescription)
            }
            self.view?.hideLoadingIndicator()
        }
    }
    
    func numberOfSchools() -> Int {
        return viewModels.count
    }
    
    func school(at index: Int) -> SchoolViewModel {
        return viewModels[index]
    }
    
    func didSelectSchool(at index: Int) {
        router.showSchoolDetails(entity: getEntity(at: index).require(), userLocation: userLocation)
    }
    
    func didBookmarkSchool(at index: Int) {
        repository.addToBookmarks(entity: getEntity(at: index).require())
    }
    
    func didUnbookmarkSchool(at index: Int) {
        repository.removeFromBookmarks(entity: getEntity(at: index).require())
    }
    
    func didChangeSearchString(_ searchString: String?) {
        guard let searchString = searchString, !searchString.isEmpty else {
            searchFilter = nil
            return
        }
        if let zipCode = Int(searchString) {
            searchFilter = .zipHasPrefix(zipCode)
        } else {
            searchFilter = .nameContains(searchString)
        }
    }
    
    func didTapFilterButton() {
        router.showBoroughsFilter(selectedBoroughs: selectedBoroughs) { [weak self] boroughs in
            self?.selectedBoroughs = boroughs
        }
    }
    
    func didTapBookmarksButton() {
        router.showBookmarks()
    }
    
    func didRefresh() {
        updateViewModels()
    }
}

private extension SchoolsPresenter {
    func startLocationManager() {
        locationManager.authorized = { [weak self] in
            self?.updateViewModels()
        }
        locationManager.locationUpdated = { [weak self] location in
            guard let self = self else { return }
            if self.userLocation == nil && location != nil {
                self.userLocation = location
                self.updateViewModels()
            } else {
                self.userLocation = location
            }
        }
        locationManager.startUpdatingLocation()
    }
    
    func updateViewModels() {
        let sortDescriptor: SchoolSortDescriptor
        if let userCoordinate = userLocation?.coordinate {
            sortDescriptor = .distance(to: userCoordinate)
        } else {
            sortDescriptor = .name
        }
        
        var filters: [SchoolFilterDescriptor] = []
        if mode == .bookmarks {
            filters.append(.isBookmark)
        }
        if let searchFilter = searchFilter {
            filters.append(searchFilter)
        }
        if let boroughs = selectedBoroughs, boroughs.count > 0 && boroughs.count < Borough.allCases.count {
            filters.append(.inBorougs(boroughs))
        }
        viewModels = schoolEntities
            .apply(filters: filters, sortDescriptor: sortDescriptor)
            .map { $0.viewModel(userCoordinate: userLocation?.coordinate) }
    }
    
    func getEntity(at index: Int) -> SchoolEntity? {
        let id = viewModels[index].id
        return schoolEntities.first { $0.dbn == id }
    }
}

private extension SchoolEntity {
    // TODO: if had more time, would use Localizable.strings, preferably with swiftgen
    private func stringValue(_ optionalString: String?) -> String {
        return optionalString ?? "N/A"
    }
    
    func viewModel(userCoordinate: CLLocationCoordinate2D?) -> SchoolViewModel {
        var distanceString: String?
        if let userCoordinate = userCoordinate {
            let userLocation = CLLocation(latitude: userCoordinate.latitude, longitude: userCoordinate.longitude)
            distanceString = String(format: "%.1f miles", distance(to: userLocation).miles)
        }
        return SchoolViewModel(id: dbn,
                               name: name,
                               borough: stringValue(borough?.capitalized),
                               isBookmark: isBookmark,
                               distanceToUser: distanceString)
    }
}
