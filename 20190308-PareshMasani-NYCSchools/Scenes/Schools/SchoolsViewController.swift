//
//  SchoolsView.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

protocol SchoolsViewProtocol: class {
    func configureForBookmarks()
    func reloadList()
    func setShowsBookmarksPlaceholder(_ showsPlaceholder: Bool)
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func showError(_ error: String)
}

final class SchoolsViewController: UITableViewController, SchoolsViewProtocol, StoryboardIdentifiable {
    
    // I'm using dependency injection here which allows to mock presenter in tests.
    var presenter: SchoolsPresenterProtocol!
    
    private lazy var dataSource = SchoolsTableDataSource(reuseIdentifier: "SchoolCell", presenter: self.presenter)
    
    @IBOutlet private var sectionHeader: SchoolsSectionHeaderView!
    @IBOutlet private var placeholder: UIView!
    
    private var isFirstAppear = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRefreshControl()
        tableView.dataSource = dataSource
        presenter.didLoadView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirstAppear {
            isFirstAppear = false
        } else {
            // refresh because bookmark status of some schools could change
            // NOTE: ideally I would not refresh the entire data set here, this is due to lack of time
            presenter.didRefresh()
        }
    }
    
    func configureForBookmarks() {
        navigationItem.title = "Bookmarks"
        navigationItem.rightBarButtonItems = nil
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
    }
    
    func reloadList() {
        sectionHeader.resultsLabel.text = "\(presenter.numberOfSchools()) High Schools"
        tableView.reloadData()
    }
    
    func setShowsBookmarksPlaceholder(_ showsPlaceholder: Bool) {
        tableView.backgroundView = showsPlaceholder ? placeholder : nil
        tableView.separatorStyle = showsPlaceholder ? .none : .singleLine
    }
    
    func showLoadingIndicator() {
        showActivityView()
    }
    
    func hideLoadingIndicator() {
        hideActivityView()
    }
    
    func showError(_ error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectSchool(at: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sectionHeader
    }
}

private extension SchoolsViewController {
    func setupRefreshControl() {
        // user can pull to refresh to sort the list by user's updated location
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @IBAction func filterButtonAction() {
        presenter.didTapFilterButton()
    }
    
    @IBAction func bookmarksButtonAction() {
        presenter.didTapBookmarksButton()
    }
    
    @objc func refreshAction(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        presenter.didRefresh()
    }
    
    @objc func doneAction() {
        dismiss(animated: true, completion: nil)
    }
}

// -------------------------------------
// MARK: - UISearchBarDelegate
// -------------------------------------
extension SchoolsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.didChangeSearchString(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

// -------------------------------------
// MARK: - UITableViewDataSource
// -------------------------------------
final class SchoolsTableDataSource: NSObject, UITableViewDataSource {
    let reuseIdentifier: String
    let presenter: SchoolsPresenterProtocol
    
    init(reuseIdentifier: String, presenter: SchoolsPresenterProtocol) {
        self.reuseIdentifier = reuseIdentifier
        self.presenter = presenter
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? SchoolTableViewCell).require()
        let school = presenter.school(at: indexPath.row)
        cell.nameLabel.text = school.name
        cell.boroughLabel.text = school.borough
        cell.distanceLabel.text = school.distanceToUser
        cell.isBookmark = school.isBookmark
        cell.bookmarkPressed = { [weak self] bookmarked in
            if bookmarked {
                self?.presenter.didBookmarkSchool(at: indexPath.row)
            } else {
                self?.presenter.didUnbookmarkSchool(at: indexPath.row)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfSchools()
    }
}

final class SchoolsSectionHeaderView: UIView {
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var resultsLabel: UILabel!
}
