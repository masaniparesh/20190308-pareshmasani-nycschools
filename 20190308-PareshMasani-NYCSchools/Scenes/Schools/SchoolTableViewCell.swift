//
//  SchoolTableViewCell.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

final class SchoolTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var boroughLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet private var bookmarkButton: UIButton! 
    
    var isBookmark = false {
        didSet {
            bookmarkButton.isSelected = isBookmark
        }
    }
    
    typealias BookmarkAction = (Bool) -> Void
    
    var bookmarkPressed: BookmarkAction?
    
    @IBAction private func bookmarkAction() {
        isBookmark = !isBookmark
        bookmarkPressed?(isBookmark)
    }
}
