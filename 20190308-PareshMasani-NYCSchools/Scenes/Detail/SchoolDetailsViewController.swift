//
//  SchoolsView.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

protocol SchoolDetailsViewProtocol: class {
    func populate(with viewModel: SchoolDetailViewModel)
}

final class SchoolsDetailsViewController: UIViewController, SchoolDetailsViewProtocol, StoryboardIdentifiable {
    
    var presenter: SchoolDetailsPresenterProtocol!
    
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var bookmarkButton: UIButton!
    @IBOutlet private var addressLabel: UILabel!
    @IBOutlet private var milesLabel: UILabel!
    @IBOutlet private var directionsButton: UIButton!
    @IBOutlet private var testTakersLabel: UILabel!
    @IBOutlet private var mathScoreLabel: UILabel!
    @IBOutlet private var writingScoreLabel: UILabel!
    @IBOutlet private var readingScoreLabel: UILabel!
    @IBOutlet private var totalStudentsLabel: UILabel!
    @IBOutlet private var attendanceRateLabel: UILabel!
    @IBOutlet private var gradesLabel: UILabel!
    @IBOutlet private var overviewLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarButtons()
        setupDirectionsButton()
        setupScoreLabels()
        presenter.didLoadView()
    }
    
    func populate(with viewModel: SchoolDetailViewModel) {
        nameLabel.text = viewModel.name
        addressLabel.text = viewModel.address
        milesLabel.text = viewModel.distanceToUser
        testTakersLabel.text = viewModel.testTakers
        mathScoreLabel.text = viewModel.mathScore
        readingScoreLabel.text = viewModel.readingScore
        writingScoreLabel.text = viewModel.writingScore
        totalStudentsLabel.text = viewModel.totalStudents
        attendanceRateLabel.text = viewModel.attendancePercentage
        attendanceRateLabel.textColor = viewModel.attendancePercentageColor
        gradesLabel.text = viewModel.grades
        overviewLabel.text = viewModel.overview
        bookmarkButton.isSelected = viewModel.isBookmark
    }
   
}

private extension SchoolsDetailsViewController {
    func setupNavigationBarButtons() {
        let phoneItem = UIBarButtonItem(image: UIImage(named: "phone"), style: .plain, target: self, action: #selector(phoneAction))
        let mailItem = UIBarButtonItem(image: UIImage(named: "mail"), style: .plain, target: self, action: #selector(emailAction))
        let webItem = UIBarButtonItem(image: UIImage(named: "web"), style: .plain, target: self, action: #selector(websiteAction))
        navigationItem.rightBarButtonItems = [webItem, mailItem, phoneItem]
    }
    
    func setupDirectionsButton() {
        directionsButton.layer.borderWidth = 1.5
        directionsButton.layer.borderColor = UIColor.themeColor.cgColor
    }
    
    func setupScoreLabels() {
        setupScoreLabel(mathScoreLabel)
        setupScoreLabel(writingScoreLabel)
        setupScoreLabel(readingScoreLabel)
    }
    
    func setupScoreLabel(_ label: UILabel) {
        label.layer.borderWidth = 2
        label.layer.borderColor = UIColor.themeColor.cgColor
        label.layer.cornerRadius = label.frame.width / 2
    }
}

// -------------------------------------
// MARK: - Actions
// -------------------------------------
private extension SchoolsDetailsViewController {
    @objc func phoneAction() {
        presenter.didTapPhoneButton()
    }
    
    @objc func emailAction() {
        presenter.didTapMailButton()
    }
    
    @objc func websiteAction() {
        presenter.didTapWebsiteButton()
    }
    
    @IBAction func addressAction() {
        presenter.didTapAddressButton()
    }
    
    @IBAction func bookmarkAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.isSelected ? presenter.didBookmark() : presenter.didUnbookmark()
    }
}
