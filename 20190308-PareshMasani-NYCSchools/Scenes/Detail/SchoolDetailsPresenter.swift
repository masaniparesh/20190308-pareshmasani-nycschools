//
//  SchoolsPresenter.swift
//  20190308-PareshMasani-NYCSchools
//
//  Created by Paresh Masani on 09/03/2019.
//  Copyright © 2019 Paresh Masani. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import MapKit

protocol SchoolDetailsPresenterProtocol: class {
    func didLoadView()
    func didTapPhoneButton()
    func didTapMailButton()
    func didTapWebsiteButton()
    func didTapAddressButton()
    func didBookmark()
    func didUnbookmark()
}

final class SchoolDetailsPresenter: SchoolDetailsPresenterProtocol {
    
    weak var view: SchoolDetailsViewProtocol?
    
    let entity: SchoolEntity
    let router: SchoolDetailRouterProtocol
    let repository: SchoolRepositoryProtocol
    let userLocation: CLLocation?
    let application = UIApplication.shared
    
    init(entity: SchoolEntity,
         router: SchoolDetailRouterProtocol,
         repository: SchoolRepositoryProtocol = SchoolRepository(),
         userLocation: CLLocation?) {
        self.entity = entity
        self.router = router
        self.repository = repository
        self.userLocation = userLocation
    }
    
    func didLoadView() {
        let viewModel = entity.detailViewModel(userCoordinate: userLocation?.coordinate)
        view?.populate(with: viewModel)
    }
    
    func didTapPhoneButton() {
        if let phone = entity.phone {
            application.callPhone(phone)
        }
    }
    
    func didTapMailButton() {
        if let email = entity.email {
            application.sendEmail(to: email)
        }
    }
    
    func didTapWebsiteButton() {
        // NOTE: given more time, if there is no website, we could hide/disable the button, or show alert
        // same for email and phone
        if let url = entity.websiteURL {
            router.openSafari(url)
        }
    }
    
    func didTapAddressButton() {
        guard let coordinate = entity.coordinate else { return }
    
        let regionDistance: CLLocationDistance = 1000
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(entity.name)"
        mapItem.openInMaps(launchOptions: options)
    }
    
    func didBookmark() {
        repository.addToBookmarks(entity: entity)
    }
    
    func didUnbookmark() {
        repository.removeFromBookmarks(entity: entity)
    }
}

private extension SchoolEntity {
    private enum Const {
        // TODO: if had more time, would use Localizable.strings, preferably with swiftgen
        static let unavailable = "N/A"
    }
    
    private func stringValue(_ optionalString: String?) -> String {
        return optionalString ?? Const.unavailable
    }
    
    private func numberValue<T: BinaryInteger>(_ number: T) -> String {
        return number > 0 ? String(number) : Const.unavailable
    }
    
    func detailViewModel(userCoordinate: CLLocationCoordinate2D?) -> SchoolDetailViewModel {
        var locationString = location
        // remove coordinates from string
        if let string = locationString, let index = string.firstIndex(of: "(") {
            locationString = String(string[string.startIndex..<index])
        }
        var distanceString: String?
        if let userCoordinate = userCoordinate {
            let userLocation = CLLocation(latitude: userCoordinate.latitude, longitude: userCoordinate.longitude)
            distanceString = String(format: "%.1f miles", distance(to: userLocation).miles)
        }
        let attendanceColor: UIColor
        if attendanceRate < 0.8 {
            attendanceColor = .badAttendanceRate
        } else if attendanceRate < 0.9 {
            attendanceColor = .normalAttendanceRate
        } else {
            attendanceColor = .goodAttedanceRate
        }
        return SchoolDetailViewModel(
            name: name,
            address: stringValue(locationString),
            distanceToUser: distanceString,
            mathScore: numberValue(mathAvgScore),
            readingScore: numberValue(readingAvgScore),
            writingScore: numberValue(writingAvgScore),
            testTakers: "(\(numberOfSATTakers) test takers)",
            totalStudents: numberValue(totalStudens),
            attendancePercentage: "\(Int(attendanceRate * 100))%",
            attendancePercentageColor: attendanceColor,
            grades: stringValue(finalGrades),
            overview: stringValue(overview),
            isBookmark: isBookmark
        )
    }
    
    var websiteURL: URL? {
        guard var website = website else { return nil }
        if !website.hasPrefix("http") {
            website = "http://\(website)"
        }
        return URL(string: website)
    }
}
